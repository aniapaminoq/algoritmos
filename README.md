# Algoritmos

---

### Logica de programacion: conceptos primordiales

<details>
 <summary>  Exercises
</summary>

| N°     | Questions                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| ------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| **1**  | Crear un programa para verificar la cantidad de puntos de un equipo de fútbol, donde cada partido ganado vale 3 puntos y cada empate vale 1 punto. Además, determinar si el equipo ha mejorado o empeorado en comparación con el año anterior. Si el total de puntos es mayor a 28, el equipo está mejor que el año pasado; si es menor, está peor; y si es igual, está igual que el año pasado.                                                                                                                                                                                                                          |
| **2**  | Realizar un programa que puedo calcular el índice de masa corporal (IMC) de una persona y proporcionar su nombre, su IMC calculado y un diagnóstico de acuerdo con los siguientes criterios. Si el IMC es menor que 18.5, indicar que está por debajo de lo recomendado. Si es 18.5 pero menor que 25, considerarlo dentro del intervalo normal. Si es 25 pero menor que 30, considerarlo como sobrepeso, de lo contrario, obesidad. Tenga en cuenta que el IMC se calcula como peso dividido por la altura al cuadrado.                                                                                                  |
| **3**  | Realizar un programa con ciclo while para adivinar lanzamientos del usuario en relación a números que son ejecutados aleatoriamente por la programa y vemos si el usuario acertó o acierta ese número por una cantidad de veces definidas por el programador .                                                                                                                                                                                                                                                                                                                                                            |
| **4**  | Crear un programa para verifica todos los años en los cuales hubo mundial de la FIFA, recordando que hay cada cuatro años. con ciclo while.                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| **5**  | Crear una tabla de multiplicar con ciclo while y con el ciclo for                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |
| **6**  | Crear un programa para calcular la media de la edad de los integrantes de una familia, pedir cuántos integrantes tiene la familia , usar ciclo while . devolver su media de las edades.                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| **7**  | Programa que dibuja un triangulo con "\*"                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| **8**  | Realizar un programa de juego, que consiste en que el usario adivide el numero .que genero aleatoriamente el programa.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| **9**  | Realizar un validar de numeros repetidos . cuando genere el programa numeros aleatorios                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| **10** | Imprimir numeros pares de 1 al 100 usando un bucle while.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |
| **11** | En la prueba de estructura de datos, Felipe tenía la siguiente lista:` const heroes = ["Superman", "Thor","Batman", "Mujer Maravilla"];`El ejercicio pidió que mostrara un alert para cada ítem de la lista y lo implementó de la siguiente manera:` for( let i = 1; i <detalist heroes.length; i++) {    alert(heroes[i]);}`A pesar de creer que su código es correcto, obtuvo un 7/10 porque cometió un error. Su maestro dijo que mostraba todos los nombres excepto "Superman".¿Cuál es el problema con el código de Felipe? ¿Cómo puede cambiarlo para que muestre todos los nombres en la lista sin omitir ninguno? |
| **12** | Tenemos la siguiente lista de frutas: `const frutas = ["piña", "banana", "melón"];`Si queremos agregar un elemento más al array de frutas, ¿qué debemos hacer?                                                                                                                                                                                                                                                                                                                                                                                                                                                            |
| **14** |                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |

</details>

### Logica de programacion:practicando con juegos y animaciones

<details>
 <summary> Exercises</summary>

## **1**

Conceptos basicos de tag `<canvas></canvas>`

## **2**

Bernardo es un chico de 11 años y le gusta jugar Minecraft. Minecraft es un juego donde construyes un mundo con bloques, como si fuera un Lego virtual. Uno de los personajes principales del juego es Creeper, y Bernardo pidió un poster con esa caricatura.Creeper es un monstruo que explota cuando se aproxima a un jugador y tiene más o menos la siguiente figura: ![creeper](creeper.png) Si analizamos la figura vamos a descubrir que la misma está compuesta de una serie de rectángulos de diferentes colores: ![creeper](creeper2.png) Para facilitar un poco, las dimensiones de nuestros rectángulos (base x altura) en pixeles es el siguiente:

- Rectángulo 1: 350, 300 (cabeza).
- Rectángulos 2 y 3: 90, 90 (ojos).
- Rectángulo 4: 70, 100 (nariz).
- Rectángulos 5 y 6: 40, 110 (parte de la boca).
  Su canvas debe tener el tamaño de - 600 x 400 pixeles.
  ¿Será que consigues ayudar a Bernardo creando el poster con la imagen Creeper? A continuación encontrarás un paso a paso para guiarte mejor:
- Elemento de la lista 1
- Crear un nuevo archivo, por ejemplo creeper.html;
- Definir la etiqueta (tag) canvas
- Crear un script usando los objetos pantalla y pincel
- Diseñar los rectángulos a través de la función fillRect del pincel.
  No olvides pintar los rectángulos, aquí usamos los colores darkgreen y black ¡Manos a la obra!

## **3**

_**Diseñando una escuadra**_,Ya practicamos bastante con rectángulos, es hora de diseñar otra figura. En este nuevo desafío vamos a diseñar una escuadra (plantilla especial en forma de un triángulo isósceles usada por arquitectos e ingenieros). ![escuadra](escuadra.png)Si analizamos la figura, podemos percibir que una escuadra no es otra cosa que dos triángulos, uno dentro del otro. Recordando también que diseñar triángulos es diferente de diseñar rectángulos, pues es preciso diseñar.línea por línea. En otras palabras podemos decir que la API es diferente.

```js
const pantalla = document.querySelector("canvas");
const pincel = pantalla.getContext("2d");
pincel.beginPath();
pincel.moveTo(50, 50);
```

A partir de ahí podemos diseñar una línea:

```js
const pantalla = document.querySelector("canvas");
const pincel = pantalla.getContext("2d");
pincel.beginPath();
pincel.moveTo(50, 50);
pincel.lineTo(50, 400);
```

Una vez que diseñamos todas las líneas, podemos finalizar rellenando la figura con:`pincel.fill();` Ahora es tu turno, crea un nuevo programa (por ejemplo escuadra.html) y diseña una escuadra basada en las coordenadas de la imagen de arriba.

## **4**

Generar tres barras horizontales (green,yellow,red) conformado por cuadrados de 50x50 , en canva de ancho 600 y altura 400 , sugerencia usar funcion y bucles . ![barra](barra.PNG)

## **5**

**diseñando una flor**,Tenemos el siguiente código que declara la función `dibujarCirculo`. Esa función permite diseñar en la pantalla una circunferencia en el eje `X` e `Y`, y también nos permite definir su color

```js
<canvas width="600" height="400"></canvas>
 <script>
const pantalla = document.querySelector("canvas");
const pincel = pantalla.getContext("2d");
pincel.fillStyle = "lightgray";
pincel.fillRect(0, 0, 600, 400);
function dibujarCirculo(x, y, radio, color)
{pincel.fillStyle = color;
pincel.beginPath();
pincel.arc(x, y, radio, 0, 2*3.14);
pincel.fill();}
</script>
```

Pero en ese código la función dibujarCirculo todavía no está siendo usada, haz uso de la función para diseñar una flor conforme la imagen de abajo:![flor](flor.png)
Utiliza como punto de referencia para el centro de la flor (círculo rojo) las coordenadas X=300 y Y=200.

## **6**

**grafico de barras**
A través de gráficos podemos expresar visualmente datos o valores numéricos, y así facilitar la comprensión de la información que estamos presentando.

Existen varios tipos de gráficos, entre los más famosos están los gráficos de barras, que será objeto de estudio en este ejercicio. Encontramos algunos datos relevantes en internet sobre la evolución de uso de navegadores o exploradores en los últimos 10 años, el resultado fue el siguiente.

- En 2009: 6% Chrome, 47% Firefox, 41% Internet Explorer/Edge\*, 3% Safari, 3% Otros.
- En 2019: 81% Chrome, 9% Firefox, 3% Internet Explorer/Edge\*, 3% Safari, 4% Otros.

Para simplificar nuestro gráfico sumamos los valores de Internet Explorer e IE Edge, los dos navegadores son de la misma empresa, Microsoft.

Como podrán ver en los datos la relevancia que ganó el navegador de Google (Chrome), teniendo una supremacía del 81% en 2019.

Existen varios tipos de gráficos de barras, en este ejemplo, vamos usar las barras verticales apiladas, nuestros datos graficados se verían así:

![grafico barras](grafico_barras.png)

Tú ya aprendiste a dibujar rectángulos y ya creamos una función con ese propósito, ingresar texto dentro de nuestro canvas es sencillo también, a continuación sigue el código con la función `dibujarRectangulo` y `escribirTexto`:

```js
<canvas width="600" height="400"></canvas>

<script>

    function dibujarRectangulo(x, y, base, altura, color) {
        const pantalla = document.querySelector("canvas");
        const  pincel = pantalla.getContext("2d");

        pincel.fillStyle=color;
        pincel.fillRect(x,y, base, altura);
        pincel.strokeStyle="black";
        pincel.strokeRect(x,y, base, altura);
    }

    function escribirTexto(x , y, texto) {
        const  pantalla = document.querySelector("canvas");
        const  pincel = pantalla.getContext("2d");

        pincel.font="15px Georgia";
        pincel.fillStyle="black";
        pincel.fillText(texto, x, y);
    }


    //Aquí viene el texto faltante

</script>
```

Ya vimos también cómo representar varios valores dentro de un array. Así podemos guardar los valores de los porcentuales de cada año. En el mundo de gráficos los valores son llamados de series:

```js
const serie2009 = [6, 47, 41, 3, 3];
const serie2019 = [81, 9, 3, 3, 4];
```

Cada valor en el array representa un %.

Igualmente, podemos crear otro array con los colores usados en el gráfico:

```js
const colores = ["blue", "green", "yellow", "red", "gray"];
```

![barra](grafico_barras-2.png)

Ahora viene el desafío: escribe una función dibujarBarra que cree una barra (5 rectángulos, el primero azul, el segundo verde, etc., igual al gráfico de arriba)

La llamada a la función puede ser:

```js
dibujarBarra(50, 50, serie2009, colores, "2009");
dibujarBarra(150, 50, serie2019, colores, "2019");
```

La primer barra comienza en `X=50` y `Y=50,` y recibe la serie del 2009, los colores y el texto 2009. Basado en eso ya podemos tener una idea de cómo será nuestra función:

```js
function dibujarBarra(x, y, serie, colores, texto) {
  //Aquí necesitamos escribir el texto y dibujar los rectángulos
}
```

Dentro de esa función necesitas implementar un Loop (ciclo `for` o `while`) y acumular el valor de la serie que sería la altura de cada rectángulo de nuestra barra.

Implementa la función `dibujarBarra` y llámala dos veces para diseñar las dos barras.

Seguramente ya habrás visto que a nuestro gráfico le falta un ítem importante que son las leyendas del gráfico (Chrome es azul, Firefox verde, Internet Explorer amarillo, Safari rojo y otros plomo). Por el momento, vamos a dejar de lado las leyendas para no agregar más complejidad a nuestro gráfico.

## **7**

el desafío es el siguiente, vamos a liberar que el usuario pueda alterar el color de los círculos que son diseñados en la pantalla. Los colores que liberaremos serán azul, rojo y verde (`blue`, `red `y `green`). Esa lista de colores te debe recordar algo que ya vimos, los `arrays` .

¿Cómo dejaremos al usuario escoger el color? A cada clic del botón **DERECHO** del mouse, el color padrón que es blue, deberá cambiarse a `red`. Si el usuario realiza otro clic del botón **DERECHO**, el color del círculo se cambiará a `green`, debes respetar el orden de alteración de los colores (`blue`, `red` y `green`). En caso de que el usuario vuelva a presionar el botón **DERECHO**el color debe volver a ser `blue`.

Para que las circunferencias aparezcan seguimos manteniendo la misma lógica, ellas aparecerán con cada clic **IZQUIERDO**del mouse.

La instrucción para capturar el evento cuando el usuario hace clic en el botón derecho todavía no fue enseñada, pero es fácil de ser implementada, el comando a ser usado es `oncontextmenu`. La sintaxis es igual al evento `onclick `que usamos para capturar el clic del botón izquierdo del mouse, de todas formas te muestro a continuación cómo puedes usar esa instrucción:

```js
<canvas width="600" height="400"> </canvas>

<script>

    const  pantalla = document.querySelector("canvas");
   const  pincel = pantalla.getContext("2d");
    pincel.fillStyle = "grey";
    pincel.fillRect(0,0,600,400);

    function dibujarCirculo(evento){
        const  x = evento.pageX - pantalla.offsetLeft;
       const  y = evento.pageY - pantalla.offsetTop;
        pincel.fillStyle = "blue";
        pincel.beginPath();
        pincel.arc(x,y,10,0,2*3.14);
        pincel.fill();
        console.log(x + "," + y);
    }

    pantalla.onclick = dibujarCirculo;

    function alterarColor() {
        alert("Funcionó");
        return false;
    }

    pantalla.oncontextmenu = alterarColor;

```

Ejecuta ese programa y prueba realizar clic en el botón **DERECHO** en la pantalla. El mensaje **"Funcionó"** será exhibido. La instrucción `return` `false` es importante para que el menú contextual padrón de `canvas` no sea exhibido, o sea, queremos apenas alterar el color con el clic del botón y no exhibir un menú para el usuario.

## **8**

**¡Ya que va, que vuelva!**,En esta aula aprendimos a animar un círculo que se movía de izquierda a derecha, hasta dejar el área visible del canvas, nuestra pantalla.

Vamos a recordar nuestro código:

```js
<canvas width="600" height="400"> </canvas>

<script>
    var pantalla = document.querySelector("canvas");
    var pincel = pantalla.getContext("2d");
    pincel.fillStyle = "lightgrey";
    pincel.fillRect(0,0,600,400);

    function disenharCircunferencia(x,y,radio){
        pincel.fillStyle = "blue";
        pincel.beginPath();
        pincel.arc(x,y,radio,0,2*Math.PI);
        pincel.fill();
    }

    function limpiarPantalla(){

        pincel.clearRect(0,0,600,400);

    }

    var x = 0

    function actualizarPantalla(){

        limpiarPantalla();
        disenharCircunferencia(x,20,10);
        x++;
    }

    setInterval(actualizarPantalla,100);

</script>
```

Altera el código para que el círculo ni bien llegue al extremo derecho de la pantalla vuelva en sentido contrario (de derecha a izquierda), y cuando retorne al lugar inicial (lado izquierdo), nuevamente se mueva de izquierda a derecha, y así sucesivamente, yendo y volviendo eternamente.

## **9**

crear un juego,donde el usuario acierte en el centro de la diana. termine el juego con un mensaje.

<details>

### HTML5 Y CSS3 parte 1:Mi primera pagina web

<details>
 <summary> Exercises</summary>

## **1**

<details>
